package model;
/**
 * Defines the direction of a Fighter
 */
public enum Direction {
	LEFT,
	RIGHT;
}