package it.unibo.kickout;

import static org.junit.Assert.assertNotEquals;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import org.junit.runner.RunWith;
import com.badlogic.gdx.Gdx;

import it.unibo.kickout.GdxTestRunner;
import view.stage.AnimationManager;
import model.Status;

@RunWith(GdxTestRunner.class)
public class TestSprites {
    private AnimationManager animationManager;

    /*
     * check the existence of the first fighter's folders
     */
	@Test
	public void firstFighterFolders() {
		assertTrue(Gdx.files.internal("../core/assets/Player0/attack1").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/attack1").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player0/idle").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/idle").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player0/hit").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/hit").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player0/jump").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/jump").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player0/walk").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/walk").isDirectory());
	}
	
	/*
     * check the existence of the second fighter's folders
     */
	@Test
	public void secondFighterFolders() {
		assertTrue(Gdx.files.internal("../core/assets/Player1/attack1").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/attack1").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player1/idle").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/idle").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player1/hit").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/hit").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player1/jump").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/jump").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Player1/walk").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/walk").isDirectory());
	}
	
	/*
     * checks if the fighter's sprite folders are not empty 
     */
	@Test
	public void folderSprites() {
		//First Fighter
		assertTrue(Gdx.files.internal("../core/assets/Player0/attack1/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/hit/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/idle/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/jump/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0/walk/0.png").exists());
		//Second Fighter
		assertTrue(Gdx.files.internal("../core/assets/Player1/attack1/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/hit/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/idle/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/jump/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1/walk/0.png").exists());
	}
	
	/*
     * check if AnimationManager functions correctly
     */
	@Test
	public void testOne() {
		this.animationManager = new AnimationManager("../core/assets/Player0");
		assertNotEquals(this.animationManager.getAnimation(Status.ATTACK1).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.HIT).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.IDLE).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.JUMP).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.WALK).getKeyFrames().length, 0);
		
		this.animationManager = new AnimationManager("../core/assets/Player1");
		assertNotEquals(this.animationManager.getAnimation(Status.ATTACK1).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.HIT).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.IDLE).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.JUMP).getKeyFrames().length, 0);
		assertNotEquals(this.animationManager.getAnimation(Status.WALK).getKeyFrames().length, 0);
	}

}