package it.unibo.kickout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import utility.KeyMapper;
@RunWith(GdxTestRunner.class)
public class TestKeyMap {

	KeyMapper keyMapper1;
	KeyMapper keyMapper2;
	
	@Before
	public void init() {
		keyMapper1 = new KeyMapper("../core/assets/keyfile/keyPlayer1");
		keyMapper2 = new KeyMapper("../core/assets/keyfile/keyPlayer2");
	}
	/*
	 * Check if the keys are different between the players
	 */
	@Test
	public void keyDiffByPlayers() {
		assertNotEquals(keyMapper1.getAttack(), keyMapper2.getAttack());
		assertNotEquals(keyMapper1.getJump(), keyMapper2.getJump());
		assertNotEquals(keyMapper1.getLeft(), keyMapper2.getLeft());
		assertNotEquals(keyMapper1.getRight(), keyMapper2.getRight());
	}
	/*
	 * Check if the Player's keys are different
	 */
	@Test
	public void keyDiffByPlayer1() {
		assertNotEquals(keyMapper1.getAttack(), keyMapper1.getJump());
		assertNotEquals(keyMapper1.getAttack(), keyMapper1.getLeft());
		assertNotEquals(keyMapper1.getAttack(), keyMapper1.getRight());
		
		assertNotEquals(keyMapper1.getJump(), keyMapper1.getLeft());
		assertNotEquals(keyMapper1.getJump(), keyMapper1.getRight());
		
		assertNotEquals(keyMapper1.getLeft(), keyMapper1.getRight());
	}
	/*
	 * Check if the Player's keys are different
	 */
	@Test
	public void keyDiffByPlayer2() {
		assertNotEquals(keyMapper2.getAttack(), keyMapper2.getJump());
		assertNotEquals(keyMapper2.getAttack(), keyMapper2.getLeft());
		assertNotEquals(keyMapper2.getAttack(), keyMapper2.getRight());
		
		assertNotEquals(keyMapper2.getJump(), keyMapper2.getLeft());
		assertNotEquals(keyMapper2.getJump(), keyMapper2.getRight());
		
		assertNotEquals(keyMapper2.getLeft(), keyMapper2.getRight());
	}
	/*
	 * Check if the Player's keys are set default
	 */
	@Test
	public void Player1Default() {
		assertEquals(keyMapper1.getAttack(), 34);
		assertEquals(keyMapper1.getJump(), 51);
		assertEquals(keyMapper1.getLeft(), 29);
		assertEquals(keyMapper1.getRight(), 32);
	}
	/*
	 * Check if the Player's keys are set default
	 */
	@Test
	public void Player2Default() {
		assertEquals(keyMapper2.getAttack(), 36);
		assertEquals(keyMapper2.getJump(), 37);
		assertEquals(keyMapper2.getLeft(), 38);
		assertEquals(keyMapper2.getRight(), 40);
	}

}
