package it.unibo.kickout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import org.junit.runner.RunWith;
import com.badlogic.gdx.Gdx;

import it.unibo.kickout.GdxTestRunner;
import utility.AnimationGenerator;

@RunWith(GdxTestRunner.class)
public class TestAnimationAspect {
    private AnimationGenerator animationBars;

    /*
     * check the existence of the images for the loading screen
     */
	@Test
	public void testOne() {
		assertTrue(Gdx.files.internal("../core/assets/Loading_images").exists());
		assertTrue(Gdx.files.internal("../core/assets/Loading_images").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Loading_images/0.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Loading_Background.jpg").exists());
		assertTrue(Gdx.files.internal("../core/assets/Logo.png").exists());	
	}
	
	/*
     * check the animation for the loading bar
     */
	@Test
	public void testTwo() {
		assertTrue(Gdx.files.internal("../core/assets/Skin").exists());
		assertTrue(Gdx.files.internal("../core/assets/Skin").isDirectory());
		
		this.animationBars = new AnimationGenerator("../core/assets/Loading_images");
		assertEquals(this.animationBars.getAnimation().getKeyFrames().length, 26);
		this.animationBars.getAnimation().setFrameDuration(2);	
		assertEquals((int)this.animationBars.getAnimation().getFrameDuration(), 2);
	}
	
	/*
     * check if there are fighter images for the menu 
     */
	@Test
	public void testThree() {
		assertTrue(Gdx.files.internal("../core/assets/PlayerImage").exists());
		assertTrue(Gdx.files.internal("../core/assets/PlayerImage").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/PlayerImage/0.jpg").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player0").isDirectory());
		assertEquals(Gdx.files.internal("../core/assets/Player0").length(),0);
		
		
		assertTrue(Gdx.files.internal("../core/assets/PlayerImage/1.jpg").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1").exists());
		assertTrue(Gdx.files.internal("../core/assets/Player1").isDirectory());
		assertEquals(Gdx.files.internal("../core/assets/Player1").length(),0);
		
		assertFalse(Gdx.files.internal("../core/assets/PlayerImage/3.png").exists());
		assertFalse(Gdx.files.internal("../core/assets/Player3").exists());
		
	}
	
	/*
     * check if there are map images and the arrow images 
     */
	@Test
	public void testFour() {
		assertTrue(Gdx.files.internal("../core/assets/Map_images").exists());
		assertTrue(Gdx.files.internal("../core/assets/Map_images").isDirectory());
		assertTrue(Gdx.files.internal("../core/assets/Map_images/0.jpg").exists());
		assertFalse(Gdx.files.internal("../core/assets/Map_images/3.png").exists());
		
		assertTrue(Gdx.files.internal("../core/assets/Untitled-1.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/Untitled-2.png").exists());
		assertFalse(Gdx.files.internal("../core/assets/Untitled-3.png").exists());
	}
	
	/*
     * check if there are trap image and the platform images 
     */
	@Test
	public void testFive() {
		assertTrue(Gdx.files.internal("../core/assets/trap.png").exists());
		
		assertTrue(Gdx.files.internal("../core/assets/1200px-SSBU-Battlefield.png").exists());
		assertTrue(Gdx.files.internal("../core/assets/images.png").exists());
	}
}